package com.rss.api.logic;

import java.util.LinkedList;
import java.util.List;

public class URLController {
	
	private static URLController instance;
	private List<String> urls;

	
		private URLController() {
    	urls = new LinkedList<String> ();
		}
	
		public static URLController getInstance() {
		  if (instance == null){
	        	instance = new URLController();
	        }
	        return instance;
		}
	
	 	void addURL(String newUrl) {
	    	urls.add(newUrl);
	    }
	    
	 	List<String> getUrls () {
	    	return urls;
	    }

}
