package com.rss.api.logic;
import java.util.List;

import com.rss.api.model.*;

/**
 * 
 * @author Leguizam�n Marcos - Utizi Sebasti�n
 * Punto de entrada a la API.
 */

public class APIMainController implements InterfaceAPI{
	
	private static APIMainController instance;
	private APINewsParser apinews;
	private URLController urlcontroller;
	
    private APIMainController() {
    	apinews = APINewsParser.getInstance();
    	urlcontroller = URLController.getInstance();
    }

     public static APIMainController getInstance() {
    	if (instance==null) {
    		instance = new APIMainController();
    	}
    	return instance;
    }

	@Override
	public List<APIFeed> getNews() {
		return apinews.getNews();
	}

	@Override
	public void addNewUrl(String link) {
		urlcontroller.addURL(link);
		
	}
	
	
	
	
	
}
