package com.rss.api.logic;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.rss.api.model.*;


class APINewsParser{
	
	private List <APIFeed> feed;
	private static APINewsParser instance;
	private URLController urlcontroller;
	
	 private APINewsParser() {
	    	urlcontroller = URLController.getInstance();
	    	feed = new LinkedList<APIFeed>();
	    }

	     static APINewsParser getInstance() {
	        if (instance == null){
	        	instance = new APINewsParser();
	        }
	        
	        return instance;
	    }
	
	public List<APIFeed> getNews() {
		try {
			List<String> urls = urlcontroller.getUrls();
			for(String urlString : urls){
				URL url = new URL(urlString);
				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = null;
	
	
				db = dbf.newDocumentBuilder();
	
				Document doc = db.parse(url.openStream());
				NodeList items = doc.getDocumentElement().getElementsByTagName("item");
				for(int i = 0; i < items.getLength(); i++){
					Element item = (Element)items.item(i);
					Element title =  (Element)item.getElementsByTagName("title").item(0);
					Element link =  (Element)item.getElementsByTagName("link").item(0);
					feed.add(new APIFeed(title.getTextContent(),link.getTextContent()));
				}
			}
		}
		catch (SAXException e) {
			// handle SAXException
		} 
		catch (IOException e) {
			// handle IOException
		} 
		catch (ParserConfigurationException e1) {
			// handle ParserConfigurationException
		}	
		return feed;
	}
}
