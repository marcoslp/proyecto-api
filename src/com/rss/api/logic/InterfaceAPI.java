package com.rss.api.logic;
import java.util.List;

import com.rss.api.model.*;

public interface InterfaceAPI {
	
		public List<APIFeed> getNews();
		public void addNewUrl(String link);
}
