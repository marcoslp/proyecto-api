package com.rss.api.model;

/**
 * 
 * @author Leguizamón Marcos - Utizi Sebastián
 * Abstracción del objeto de dato noticia.
 */

public class APIFeed {
	private String title;
	private String link;
	
	public APIFeed(String title, String link){
		this.setTitle(title);
		this.setLink(link);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
