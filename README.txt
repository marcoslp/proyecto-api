
#---------------------------------------------------
#
#	API-RSS
#
#	Marcos Leguizamon - Utizi Sebastian
#
#---------------------------------------------------


#---------------------------------------------------
	
	La API brinda los siguientes m�todos en su interface p�blica:
	
		- getNews(): APIFeed[]
		- void addNewUrl(linkRSS: String): void
	
	Para poder usar estos m�todos se debe importar el paquete:
		com.newsreaders.api.logic.*;
	
	Los m�odos mencionados estan disponibles en la interface llamada: InterfaceAPI

	IMPORTANTE: Recordar tener el .jar de la API en el Build Path de su proyecto.
		
#---------------------------------------------------
	
	El uso de la RSSAPI ser� a trav�s de un conector:

package com.newsreader.news.model;
import com.newsreaders.api.logic.*;

public class APIConector {
	private InterfaceAPI api;
	private static APIConector instance;
	       
    private APIConector () {
    	api = APIMainController.getInstance();
	}
	 public static APIConector getInstance() {
		 if (instance==null) {
			 instance = new APIConector();
	     }
		 return instance;
	}

     public InterfaceAPI getRSSAPI() {
         return api;
     }
}


	Ejemplo de uso:
	
		import com.newsreaders.api.logic.*;		
		InterfaceAPI api = APIConector.getInstance().getRSSAPI(); //solicito al conector la instancia de la api
		api.addNewUrl("http://www.rollingstone.com/rss"); //a�ado una url a la api (permite varias)
		Feed[] feed = api.getNews();
		//el newsreader consulta a la api sobre las noticias y esta se las devuelve correctamente.


